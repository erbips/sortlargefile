package com.here;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This program is used to Sort file data but with very less memory(maxLines is
 * consider as memory for us)
 * 
 * @author anghan
 *
 */
public class SortLargeFile {

	private int maxLinePerFile = 10; // Here i am assuming that 30 entry will allocate my memory.

	private List<File> outputs = new ArrayList<File>();

	private String pattern = "dd-MM-yyyy";

	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

	private String fileDirectory = "/Users/anghan/Documents/HAD-Workflow/General/src/com/here/";

	private String fileName = "test.txt";

	private String newFileName = "sorted_data_" + System.currentTimeMillis() + ".txt";

	private CustomComparator customComparator = new CustomComparator();

	private class CustomComparator implements Comparator<String> {

		@Override
		public int compare(String o1, String o2) {
			try {
				String[] data1 = o1.split(",");
				String[] data2 = o2.split(",");
				Date date1 = simpleDateFormat.parse(data1[0]);
				Date date2 = simpleDateFormat.parse(data2[0]);
				return date1.compareTo(date2);
			} catch (Exception e) {
				System.out.println("Error:Data is not correct...");
			}
			return 0;
		}

	}

	public static void main(String[] args) {
		SortLargeFile sortFile = new SortLargeFile();
		String fileFullPath = "";
		// args = new String[1];
		// args[0] = sortFile.fileDirectory + sortFile.fileName;
		if (args != null && args.length > 0) {
			fileFullPath = args[0];
			sortFile.fileName = fileFullPath.substring(fileFullPath.lastIndexOf("/") + 1, fileFullPath.length());
			sortFile.fileDirectory = fileFullPath.substring(0, fileFullPath.lastIndexOf("/") + 1);
			try {
				// First split your large file in differnt file base on maxLinePerFile
				InputStream stream = new FileInputStream(fileFullPath);
				sortFile.splitFile(stream);
				// to merge file
				OutputStream outStream = new FileOutputStream(sortFile.fileDirectory + sortFile.newFileName);
				sortFile.mergeFiles(outStream);
				// TODO - we can delete old file and rename new file to old name so that same
				// file can available

			} catch (FileNotFoundException f) {
				System.out.println("Error:File Not Exist..!" + fileFullPath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Error:No File Path Provided in args.");
		}

	}

	/**
	 * 
	 * Reads the input io stream and splits it into sorted files
	 * 
	 * @param in
	 * @throws IOException
	 * 
	 */
	public void splitFile(InputStream in) throws IOException {

		outputs.clear();
		BufferedReader br = null;
		List<String> lines = new ArrayList<String>();
		try {
			br = new BufferedReader(new InputStreamReader(in));
			String line = null;
			int totalLineCount = 0;
			while ((line = br.readLine()) != null) {

				lines.add(line);
				totalLineCount++;
				if (totalLineCount >= maxLinePerFile) {
					totalLineCount = 0;
					Collections.sort(lines, customComparator);
					File file = new File(fileDirectory + "log_" + System.currentTimeMillis());
					outputs.add(file);
					writeFile(lines, new FileOutputStream(file));
					lines.clear();
				}
			}

			// write out the remaining lines
			if (lines != null && !lines.isEmpty()) {
				Collections.sort(lines, customComparator);
				File file = new File(fileDirectory + "log_" + System.currentTimeMillis());
				outputs.add(file);
				writeFile(lines, new FileOutputStream(file));
				lines.clear();
			}
		} catch (IOException io) {
			throw io;
		} finally {

			if (br != null)
				try {
					br.close();
				} catch (Exception e) {
				}
		}

	}

	/**
	 * 
	 * Writes the list of lines out to the output stream, append new lines after
	 * each line.
	 * 
	 * @param list
	 * @param os
	 * @throws IOException
	 * 
	 */
	private void writeFile(List<String> list, OutputStream os) throws IOException {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(os));
			for (String s : list) {
				writer.write(s);
				writer.write("\n");
			}
			writer.flush();
		} catch (IOException io) {
			throw io;
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (Exception e) {
				}
			}

		}
	}

	/**
	 * 
	 * Reads the temporary files created by splitFiles method and merges them in a
	 * sorted manner into the output stream.
	 * 
	 * @param list
	 * @param os
	 * @throws IOException
	 * 
	 */
	public void mergeFiles(OutputStream os) throws IOException {
		Map<String, BufferedReader> map = new HashMap<String, BufferedReader>();
		List<BufferedReader> readers = new ArrayList<BufferedReader>();
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(os));
			for (int i = 0; i < outputs.size(); i++) {
				BufferedReader reader = new BufferedReader(new FileReader(outputs.get(i)));
				readers.add(reader);
				String line = reader.readLine();
				if (line != null) {
					map.put(new String(line), readers.get(i));
				}
			}

			/// continue to loop until no more lines lefts
			List<String> sorted = new LinkedList<String>(map.keySet());

			while (map.size() > 0) {
				Collections.sort(sorted, customComparator);
				String line = sorted.remove(0);
				writer.write(line);
				writer.write("\n");
				BufferedReader reader = map.remove(line);
				String nextLine = reader.readLine();
				if (nextLine != null) {
					String sw = new String(nextLine);
					map.put(sw, reader);
					sorted.add(sw);
				}
			}

		} catch (IOException io) {
			throw io;
		} finally {
			for (int i = 0; i < readers.size(); i++) {
				try {
					readers.get(i).close();
				} catch (Exception e) {
				}
			}
			for (int i = 0; i < outputs.size(); i++) {
				outputs.get(i).delete();
			}
			try {
				writer.close();
			} catch (Exception e) {
			}
		}

	}

}