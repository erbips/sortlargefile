## Synopsis

**Sort Large File**   
This project is used to sort large file.


## Technologies

1. Java 8

## Installation

## Way-1
Run `javac SortLargeFile.java` to create class file.

Run `java SortLargeFile <fileFullPath>` to execute class.

<fileFullPath> -- give you full path with file name

## Way-2
1. open eclipse or any java IDE
2. create normal java project
3. import SortLargeFile in project (make sure package name com.here or else you can change package name in SortLargeFile)
4. set variable `fileDirectory` from SortLargeFile class (give directory path which has large file)
5. set variable `fileName` for filename.
6. run program using right click (it will create sorted file at same fileDirectory location)

## Notes 
1. I am not deleting old file after the execution of program 
2. sorted file will be created which will be store at same location and file name would be `sorted_data_<current_timestamp>.txt`

## example 
javac SortLargeFile.java
java SortLargeFile /Users/anghan/Documents/General/src/com/here/test.txt

## Assumption
1. Here i assuming that all data store in file are correct format like `<Date>,<Normal Text Without , >`
2. Date format is fix dd-MM-YYYY (no HR,MNT,SEC)


